﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsController : MonoBehaviour
{
    public Animator _animator;
    public string _animationTrigger;
    public string _idleTrigger;
    public AudioSource _audioSource;
    public List<Animation> _animationsToEnable = new List<Animation>();

    private bool isFirstTime = true; 

    private Animator m_subtitlesAnimator;
    private Animator subtitlesAnimator
    {
        get
        {
            if (m_subtitlesAnimator == null)
            {
                GameObject s = GameObject.FindGameObjectWithTag("Subtitles");
                if (s != null)
                {
                    m_subtitlesAnimator = s.GetComponent<Animator>();
                }
            }
            return m_subtitlesAnimator;
        }
    }
    private Animator m_buttonsAnimator;
    private Animator buttonsAnimator
    {
        get
        {
            if (m_buttonsAnimator == null)
            {
                GameObject s = GameObject.FindGameObjectWithTag("ButtonsHolder");
                if (s != null)
                {
                    m_buttonsAnimator = s.GetComponent<Animator>();
                }
            }
            return m_buttonsAnimator;
        }
    }

    // referencias animator y nombre trigger
    public void PlayAnimationsOnClick()
    {
        // TODO: Play some SFX
        PlayAnimations();
    }

    public void PlayAnimations()
    {
        StartCoroutine(PlayAnimationsCoroutine());
    }

    public void StopAnimations()
    {
        _animator.SetTrigger(_idleTrigger);
        _audioSource?.Stop();
        EnableAnimationComponents(false);
        // Deactivate Subtitles
        if (subtitlesAnimator != null)
        {
            subtitlesAnimator.enabled = false;
        }
        if (buttonsAnimator != null)
        {
            buttonsAnimator.enabled = false;
        }
    }

    public void RestartAnimations()
    {
        if (isFirstTime)
        {
            StartCoroutine(RestartAnimationsCoroutine());
            isFirstTime = false;
        }
        else
        {
            StopAnimations();
            PlayAnimations();
        }
    }

    // Hacky way of restarting the crazy animation from the beginning, emulating 2 interactions
    private IEnumerator RestartAnimationsCoroutine()
    {
        StopAnimations();
        yield return StartCoroutine(PlayAnimationsCoroutine());
        yield return null;
        RestartAnimations();
    }

    private IEnumerator PlayAnimationsCoroutine()
    {
        // Disable all components before starting the animation
        EnableAnimationComponents(false);
        // Jump a frame
        yield return null;

        _animator.SetTrigger(_animationTrigger);
        _audioSource?.Play();
        // Enable components in the same frame
        EnableAnimationComponents(true);

        // Activate Subtitles
        if (subtitlesAnimator != null)
        {
            subtitlesAnimator.enabled = true;
            // This force to play the animation from the beginning
            subtitlesAnimator.Play("Subtitles", -1, 0f);
        }
        if (buttonsAnimator != null)
        {
            buttonsAnimator.enabled = true;
            // This force to play the animation from the beginning
            buttonsAnimator.Play("ButtonsHolder", -1, 0f);
        }
    }

    private void EnableAnimationComponents(bool shouldEnable)
    {
        if (_animationsToEnable != null && _animationsToEnable.Count > 0)
        {
            foreach (var component in _animationsToEnable)
            {
                component.Rewind();
                component.enabled = shouldEnable;
            }
        }
    }
}
