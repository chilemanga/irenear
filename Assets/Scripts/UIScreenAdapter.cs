﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScreenAdapter : MonoBehaviour
{
    [Range(0, 1f)]
    public float heightPercentage = .15f;
    public Image _image;

    private void Start()
    {
        SetImageSize();
    }
    public void SetImageSize()
    {
        float width = Screen.width;
        float height = Screen.height;
        height *= heightPercentage;
        _image.rectTransform.sizeDelta = new Vector2(width, height);
    }

#if UNITY_EDITOR
    private void Update()
    {
        SetImageSize();
    }
#endif
}
