﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class Flash : MonoBehaviour
{
    private CanvasGroup _canvasGroup;
    private CanvasGroup canvasGroup
    {
        get
        {
            if (_canvasGroup == null)
            {
                _canvasGroup = GetComponent<CanvasGroup>();
            }
            return _canvasGroup;
        }
    }

    public float FlashDuration = 0.3f;
    private float elapsedFlashDuration = 0;
    public bool IsFlashing;

    public void DoFlash()
    {
        canvasGroup.alpha = 1f;
        IsFlashing = true;
    }

    private void Update()
    {
        if (!IsFlashing) return;

        canvasGroup.alpha = 1f - (elapsedFlashDuration / FlashDuration);
        elapsedFlashDuration += Time.deltaTime;
        if (elapsedFlashDuration >= FlashDuration)
        {
            IsFlashing = false;
            canvasGroup.alpha = 0;
            elapsedFlashDuration = 0;
        }
    }
}
