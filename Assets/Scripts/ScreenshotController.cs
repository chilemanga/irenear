﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotController : MonoBehaviour
{
    [SerializeField] private Canvas _canvasToHide;
    [SerializeField] private List<GameObject> _objectsToHide;
    [SerializeField] private ShowToast toastHandler;

    private const string albumName = "Realidad Aumentada";
    private const string fileFormatName = "Morro Arica {0}.png";

    public void TakeScreenshot()
    {
        StartCoroutine(TakeScreenshotCoroutine());
    }

    private IEnumerator TakeScreenshotCoroutine()
    {
        // Hide everything that shouldn't be in the picture
        DisplayPhotobombingObjects(false);

        // Jump a frame
        yield return null;

        // Say cheese!
        //string fileName = GetScreenShotName();
        //ScreenCapture.CaptureScreenshot(fileName);

        // Take a screenshot and save it to Gallery/Photos
        yield return StartCoroutine(TakeScreenshotAndSave());

        if (toastHandler != null)
        {
            //toastHandler.ShowToastMessage("Imagen guardada en: " + fileName);
            toastHandler.ShowToastMessage("Imagen guardada en galería bajo el álbum " + albumName);
        }

        yield return null;

        // Get the hidden objects back
        DisplayPhotobombingObjects(true);
    }

    private void DisplayPhotobombingObjects(bool shouldDisplay)
    {
        if (_canvasToHide != null)
        {
            _canvasToHide.enabled = shouldDisplay;
        }
        if (_objectsToHide != null)
        {
            foreach (var o in _objectsToHide)
            {
                o.SetActive(shouldDisplay);
            }
        }
    }

    [System.Obsolete("Using NativeGallery plugin instead")]
    public static string GetScreenShotName()
    {
        string fileName = string.Format("screen_{0}.png",
            System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
        Debug.Log("Screenshot file name is here: " + Application.persistentDataPath + ", filename: " + fileName);
        return fileName;
    }

    private IEnumerator TakeScreenshotAndSave()
    {
        yield return new WaitForEndOfFrame();

        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        // Save the screenshot to Gallery/Photos
        Debug.Log("Permission result: " + NativeGallery.SaveImageToGallery(ss, albumName, fileFormatName));

        // To avoid memory leaks
        Destroy(ss);
    }
}
