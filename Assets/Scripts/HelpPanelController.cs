﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class HelpPanelController : MonoBehaviour, ITrackableEventHandler
{
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private float waitForHelpTime;
    [SerializeField] private Texture2D imageToSave;
    [SerializeField] private ShowToast toastHandler;
    [SerializeField] TrackableBehaviour mTrackableBehaviour;

    private float idleTime = 0;
    private const float fadeTime = 0.65f;
    private float fadeTimeElapsed = 0;
    private const string albumName = "Realidad Aumentada";
    private const string fileFormatName = "Morro Arica escanear {0}.png";
    private bool trackingFound = false;

    private void Start()
    {
        DisplayCanvasGroup(false);

        if (mTrackableBehaviour != null)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }

    private void DisplayCanvasGroup(bool shouldDisplay)
    {
        canvasGroup.alpha = shouldDisplay ? 1f : 0;
        canvasGroup.blocksRaycasts = shouldDisplay;
        canvasGroup.interactable = shouldDisplay;
    }

    public void OnButtonSavePressed()
    {
        NativeGallery.SaveImageToGallery(imageToSave, albumName, fileFormatName);
        if (toastHandler != null)
        {
            toastHandler.ShowToastMessage("Imagen a escanear guardada en galería bajo el álbum " + albumName);
        }
        ClosePanel();
    }

    private void Update()
    {
        if (trackingFound)
        {
            return;
        }

        if (canvasGroup.alpha >= 1f)
        {
            return;
        }
        idleTime += Time.deltaTime;
        if (idleTime >= waitForHelpTime)
        {
            fadeTimeElapsed += Time.deltaTime;
            canvasGroup.alpha = Mathf.Lerp(fadeTimeElapsed, fadeTime, Time.deltaTime);
            if (canvasGroup.alpha >= 1f)
            {
                DisplayCanvasGroup(true);
            }
        }
    }

    public void ClosePanel()
    {
        idleTime = 0;
        fadeTimeElapsed = 0;
        DisplayCanvasGroup(false);
    }

    #region PUBLIC_METHODS

    /// <summary>
    ///     Implementation of the ITrackableEventHandler function called when the
    ///     tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS

    #region PROTECTED_METHODS

    protected virtual void OnTrackingFound()
    {
        trackingFound = true;
        DisplayCanvasGroup(false);
    }


    protected virtual void OnTrackingLost()
    {
        ClosePanel();
    }

    #endregion // PROTECTED_METHODS
}
