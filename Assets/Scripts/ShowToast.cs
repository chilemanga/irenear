﻿using UnityEngine;
using System.Collections;

public class ShowToast : MonoBehaviour
{
    string toastString;
    AndroidJavaObject currentActivity;

    public void ShowToastMessage(string message)
    {
        Debug.Log("Toast: Trying to show '" + message + "' as a toast message.");
        if (Application.platform == RuntimePlatform.Android)
        {
            showToastOnUiThread(message);
        }
    }

    void showToastOnUiThread(string message)
    {
        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        this.toastString = message;

        currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
    }

    void showToast()
    {
        Debug.Log("Running on UI thread... displaying toast: "+this.toastString);
        AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
        AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
        AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
        AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_LONG"));
        toast.Call("show");
    }

}